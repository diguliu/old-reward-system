class RewardsController < ApplicationController
  rescue_from ActiveRecord::RecordInvalid, with: :render_error
  rescue_from Customer::SelfInvitationError, with: :render_error
  rescue_from Customer::CyclicalInvitationError, with: :render_error


  def perform
    return render json: 'No file provided!', status: :unprocessable_entity if params[:file].blank?

    r = Request.create!(input: params[:file])
    r.parse_actions
    r.perform_actions
    r.customers.map(&:save!) #FIXME

    render json: r.scores
  end

  private

  def render_error(error)
    render json: "Invalid input: #{error.message}", status: :unprocessable_entity
  end
end
