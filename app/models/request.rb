class Request < ApplicationRecord
  has_many :actions
  has_many :customers
  has_one_attached :input

  validates_associated :actions

  def parse_actions
    input.download.strip.split("\n").map do |line|
      date, time, source_name, verb, target_name = line.strip.split(' ')
      source = customers.find_or_create_by(name: source_name)
      target = target_name.present? ? customers.find_or_create_by(name: target_name) : nil

      actions.create!({
        datetime: "#{date} #{time}", 
        source: source,
        verb: verb,
        target: target
      })
    end
  end

  def perform_actions
    actions.order(:datetime).map(&:perform)
  end

  def scores
    customers.inject({}) do |result, customer|
      result[customer.name] = customer.score if customer.score > 0
      result
    end
  end
end
