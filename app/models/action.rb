class Action < ApplicationRecord
  belongs_to :source, class_name: 'Customer'
  belongs_to :request
  belongs_to :target, class_name: 'Customer', optional: true

  validates :datetime, :verb, presence: true
  validates :verb, inclusion: { in: %w(recommends accepts) }
  validates :target, presence: true, if: -> { verb == 'recommends' }
  validates :target, absence: true, if: -> { verb == 'accepts' }

  def perform_accepts
    source.accept_invitation
  end

  def perform_recommends
    target.set_invitation(source)
  end

  def perform
    self.send("perform_#{verb}")
  end
end
