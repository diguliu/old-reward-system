class Customer < ApplicationRecord
  class SelfInvitationError < StandardError
    def message
      "Customer can not invite himself!"
    end
  end

  class CyclicalInvitationError < StandardError
    def message
      "Invitations can't form a cyclical dependency!"
    end
  end

  belongs_to :invited_by, class_name: 'Customer', optional: true
  belongs_to :request

  validates :name, presence: true
  validate :self_invitation
  validate :cyclical_invitation

  MODIFIER = 2.0

  def self_invitation
    raise SelfInvitationError if invited_by == self
  end

  def verify_invitation_loop
    visited = []
    customer = invited_by
    while customer
      return true if visited.include?(customer)
      visited << customer
      customer = customer.invited_by
    end
  end

  def cyclical_invitation
    raise CyclicalInvitationError if verify_invitation_loop
  end

  def set_invitation(inviter)
    self.invited_by ||= inviter
    self.save!
  end

  def award(value)
    value = value / MODIFIER
    self.score += value
    self.save!

    invited_by.award(value) if invited_by
  end

  def accept_invitation
    if !accepted_invitation && invited_by
      self.accepted_invitation = true
      self.save!

      # Setting the award to MODIFIER as initial value is a trick to ensure
      # it'll become 1 on the first level and also work conveniently on further
      # levels.
      invited_by.award(MODIFIER)
    end
  end
end
