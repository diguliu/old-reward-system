class CreateActions < ActiveRecord::Migration[6.0]
  def change
    create_table :actions do |t|
      t.references :source
      t.references :target
      t.references :request
      t.string :verb
      t.datetime :datetime
      t.boolean :processed, default: false

      t.timestamps
    end
  end
end
