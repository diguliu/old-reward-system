class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.float :score, default: 0.0
      t.references :invited_by
      t.references :request
      t.boolean :accepted_invitation, default: false

      t.timestamps
    end
  end
end
