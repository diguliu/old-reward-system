require 'rails_helper'

RSpec.describe RewardsController, type: :controller do
  let(:parsed_body) { JSON.parse(response.body) }

  describe "POST #perform" do
    it "fails due to no file input" do
      post :perform
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it "fails due to an invalid input" do
      post :perform, params: { file: fixture_file_upload('invalid.txt') }
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it "fails due to self invitation" do
      post :perform, params: { file: fixture_file_upload('self-invitation.txt') }
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it "fails due to cyclical invitation" do
      post :perform, params: { file: fixture_file_upload('cyclical-invitation.txt') }
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it "calculate scores for main test case" do
      post :perform, params: { file: fixture_file_upload('main.txt') }

      expect(response).to have_http_status(:success)
      expect(parsed_body['A']).to eql(1.75)
      expect(parsed_body['B']).to eql(1.5)
      expect(parsed_body['C']).to eql(1.0)
    end
  end
end
