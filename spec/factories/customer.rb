FactoryBot.define do
  factory :customer do
    name { Faker::Name.name }
    request { create(:request) }
  end
end
