FactoryBot.define do
  factory :action do
    source { create(:customer) }
    target { create(:customer) }
    request { create(:request) }
    verb { "recommends" }
    datetime { "2019-09-11 22:20:27" }
    processed { false }
  end
end
