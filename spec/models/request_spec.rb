require 'rails_helper'

RSpec.describe Request, type: :model do
  describe 'associations' do
    it { should have_many(:actions) }
    it { should have_many(:customers) }
  end

  describe '#parse_actions' do
    subject { create(:request, input: input) }
    let(:actions) { subject.actions }
    let(:action) { actions.first }

    context 'single line invalid action' do
      let(:input) { fixture_file_upload('one-line-invalid.txt') }
      it { expect{ subject.parse_actions }.to raise_error(ActiveRecord::RecordInvalid) }
    end

    context 'single line invalid action' do
      let(:input) { fixture_file_upload('multi-line-invalid.txt') }
      it { expect{ subject.parse_actions }.to raise_error(ActiveRecord::RecordInvalid) }
    end

    context 'single line valid action' do
      let(:input) { fixture_file_upload('one-line-valid.txt') }

      before do
        subject.parse_actions
      end

      it { expect(actions.size).to        eql(1) }
      it { expect(action.datetime).to     eql(Time.zone.parse('2018-06-12 09:41')) }
      it { expect(action.source.name).to  eql('A') }
      it { expect(action.verb).to         eql('recommends') }
      it { expect(action.target.name).to  eql('B') }
      it { expect(action.valid?).to       be_truthy }

      it { expect(subject.valid?).to be_truthy }
    end

    context 'multiple lines with only valid actions' do
      let(:input) { fixture_file_upload('multi-line-valid.txt') }

      before do
        subject.parse_actions
      end

      it { expect(actions.size).to    eql(3) }

      it { expect(actions[0].datetime).to       eql(Time.zone.parse('2018-06-12 09:41')) }
      it { expect(actions[0].source.name).to    eql('A') }
      it { expect(actions[0].verb).to           eql('recommends') }
      it { expect(actions[0].target.name).to    eql('B') }
      it { expect(actions[0].valid?).to         be_truthy }

      it { expect(actions[1].datetime).to       eql(Time.zone.parse('2018-06-14 09:41')) }
      it { expect(actions[1].source.name).to    eql('B') }
      it { expect(actions[1].verb).to           eql('accepts') }
      it { expect(actions[1].target).to         be_nil }
      it { expect(actions[1].valid?).to         be_truthy }

      it { expect(actions[2].datetime).to       eql(Time.zone.parse('2018-06-16 09:41')) }
      it { expect(actions[2].source.name).to    eql('B') }
      it { expect(actions[2].verb).to           eql('recommends') }
      it { expect(actions[2].target.name).to    eql('C') }
      it { expect(actions[2].valid?).to         be_truthy }

      it { expect(subject.valid?).to be_truthy }
    end
  end

  describe '#perform_actions' do
    subject { create(:request, input: input) }
    let(:input) { fixture_file_upload('main.txt') }
    let(:a1) { double(:a1) }
    let(:a2) { double(:a2) }
    let(:a3) { double(:a3) }
    let(:actions) { double(:actions) }

    before do
      allow(actions).to receive(:order).and_return([a1, a2, a3])
      allow(subject).to receive(:actions).and_return(actions)
    end

    it do
      expect(actions).to receive(:order).with(:datetime)
      expect(a1).to receive(:perform)
      expect(a2).to receive(:perform)
      expect(a3).to receive(:perform)

      subject.perform_actions
    end
  end

  describe '#scores' do
    subject { create(:request) }
    let(:scores) { subject.scores }

    it do
      c1 = create(:customer, request: subject, score: 3.5)
      c2 = create(:customer, request: subject, score: 1.25)
      c3 = create(:customer, request: subject, score: 5.125)
      c4 = create(:customer, request: subject, score: 0)

      expect(scores).to have_key(c1.name)
      expect(scores).to have_key(c2.name)
      expect(scores).to have_key(c3.name)
      expect(scores).to_not have_key(c4.name)

      expect(scores[c1.name]).to eql(c1.score)
      expect(scores[c2.name]).to eql(c2.score)
      expect(scores[c3.name]).to eql(c3.score)
    end
  end
end
