require 'rails_helper'

RSpec.describe Action, type: :model do
  describe 'associations' do
    it { should belong_to(:source) }
    it { should belong_to(:request) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:datetime) }
    it { is_expected.to validate_presence_of(:verb) }

    it do
      is_expected.to validate_inclusion_of(:verb)
        .in_array(%w(recommends accepts))
    end

    context 'verb is recommends' do
      before { allow(subject).to receive(:verb).and_return('recommends') }
      it { is_expected.to validate_presence_of(:target) }
    end
    
    context 'verb is accepts' do
      before { allow(subject).to receive(:verb).and_return('accepts') }
      it { is_expected.to validate_absence_of(:target) }
    end

    context 'valid datetime' do
      it { is_expected.to allow_value('2018-06-12 09:41').for(:datetime) }
    end

    context 'invalid datetime' do
      it { is_expected.to_not allow_value('abcde').for(:datetime) }
    end
  end

  describe '#perform' do
    context 'accepts' do
      before { subject.verb = 'accepts' }
      it do
        expect(subject).to receive(:perform_accepts)
        subject.perform
      end
    end

    context 'recommends' do
      before { subject.verb = 'recommends' }
      it do
        expect(subject).to receive(:perform_recommends)
        subject.perform
      end
    end
  end

  describe '#perform_accepts' do
    let(:source) { double(:source)}
    before { allow(subject).to receive(:source).and_return(source) }

    it do
      expect(source).to receive(:accept_invitation)
      subject.perform_accepts
    end
  end

  describe '#perform_recommends' do
    let(:source) { double(:source)}
    let(:target) { double(:target)}

    before do
      allow(subject).to receive(:source).and_return(source)
      allow(subject).to receive(:target).and_return(target)
    end

    it do
      expect(target).to receive(:set_invitation).with(source)
      subject.perform_recommends
    end
  end
end
