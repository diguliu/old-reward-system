require 'rails_helper'

RSpec.describe Customer, type: :model do
  subject { build(:customer) }

  describe 'associations' do
    it { is_expected.to belong_to(:request) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }

    it 'verifies self invitations' do
      subject.invited_by = subject
      expect { subject.valid? }.to raise_error(Customer::SelfInvitationError)
    end

    it 'verifies cyclical invitations' do
      b = build(:customer)
      c = build(:customer)

      b.invited_by = c
      c.invited_by = subject
      subject.invited_by = b

      expect { subject.valid? }.to raise_error(Customer::CyclicalInvitationError)
    end
  end

  describe '#score' do
    it { expect(subject.score).to be == 0 }
  end

  describe '#accepted_invitation' do
    it { expect(subject.accepted_invitation).to be_falsey }
  end

  describe '#set_invitation' do
    let(:inviter) { build(:customer) }
    let(:late_inviter) { build(:customer) }

    it 'sets invited_by' do
      subject.set_invitation(inviter)
      expect(subject.invited_by).to eql(inviter)
    end

    it 'does not overwrite invited_by' do
      subject.set_invitation(inviter)
      subject.set_invitation(late_inviter)
      expect(subject.invited_by).to eql(inviter)
    end
  end

  describe '#award' do
    it 'increase score by value divided by MODIFIER' do
      subject.award(Customer::MODIFIER)
      expect(subject.score).to be == 1

      subject.award(Customer::MODIFIER)
      expect(subject.score).to be == 2
    end

    it 'chain awards inviters dividing the award value by a factor of MODIFIER on each step' do
      m = Customer::MODIFIER
      c1 = create(:customer)
      c2 = create(:customer, invited_by: c1)
      c3 = create(:customer, invited_by: c2)
      c4 = create(:customer, invited_by: c3)

      c4.award(m)

      expect(c4.score).to eql(m / m**1)
      expect(c3.score).to eql(m / m**2)
      expect(c2.score).to eql(m / m**3)
      expect(c1.score).to eql(m / m**4)
    end
  end

  describe '#accept_invitation' do
    let(:inviter) { create(:customer) }

    it 'keeps accepted false if not yet invited' do
      subject.accept_invitation
      expect(subject.accepted_invitation).to be_falsey
    end

    it 'changes accepted to true when invited' do
      subject.invited_by = inviter
      subject.accept_invitation

      expect(subject.accepted_invitation).to be_truthy
    end

    it 'accepts invitation and award inviter' do
      subject.invited_by = inviter
      expect(inviter).to receive(:award).with(2)

      subject.accept_invitation
    end

    it 'does not award inviter more then once' do
      subject.invited_by = inviter
      expect(inviter).to receive(:award).with(2).once

      subject.accept_invitation
      subject.accept_invitation
      subject.accept_invitation
    end
  end
end
